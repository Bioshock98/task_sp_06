package ru.pyshinskiy.tm.api.web;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import ru.pyshinskiy.tm.dto.DTOConverter;
import ru.pyshinskiy.tm.dto.ProjectDTO;
import ru.pyshinskiy.tm.dto.TaskDTO;
import ru.pyshinskiy.tm.enumerated.Status;
import ru.pyshinskiy.tm.security.UserPrincipal;
import ru.pyshinskiy.tm.service.project.IProjectService;
import ru.pyshinskiy.tm.service.task.ITaskService;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/task")
public class TaskController {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @Autowired
    private DTOConverter dtoConverter;

    @InitBinder
    public void initBinder(@NotNull final WebDataBinder webDataBinder) {
        @NotNull final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        dateFormat.setLenient(false);
        webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    @Secured("ROLE_ADMINISTRATOR")
    @GetMapping("/list")
    public String listTask(@NotNull final Model model) {
        @NotNull final List<TaskDTO> tasks = taskService.findAll().stream().map(e -> dtoConverter.toTaskDTO(e)).collect(Collectors.toList());
        model.addAttribute("tasks", tasks);
        return "task-list";
    }

    @GetMapping("/add")
    public String showAddingForm(@NotNull final Model model, @AuthenticationPrincipal @NotNull final UserPrincipal userPrincipal) {
        @NotNull final List<ProjectDTO> projects = projectService.findAll().stream().map(e -> dtoConverter.toProjectDTO(e)).collect(Collectors.toList());
        @NotNull final TaskDTO task = new TaskDTO();
        task.setUserId(userPrincipal.getId());
        model.addAttribute("task", task);
        model.addAttribute("projects", projects);
        model.addAttribute("statuses", Arrays.stream(Status.values()).map(Enum::toString).collect(Collectors.toList()));
        return "task-form";
    }

    @PostMapping("/save")
    public String saveTask(@ModelAttribute("task") @NotNull final TaskDTO task) {
        taskService.save(dtoConverter.toTask(task));
        return "redirect:/task/list";
    }

    @GetMapping("/edit")
    public String editTask(@RequestParam("taskId") @NotNull final String taskId, @NotNull final Model model) {
        @NotNull final List<ProjectDTO> projects = projectService.findAll().stream().map(e -> dtoConverter.toProjectDTO(e)).collect(Collectors.toList());
        @Nullable TaskDTO task = dtoConverter.toTaskDTO(taskService.findOne(taskId));
        model.addAttribute("task", task);
        model.addAttribute("projects", projects);
        model.addAttribute("statuses", Arrays.stream(Status.values()).map(Enum::toString).collect(Collectors.toList()));
        return "task-form";
    }

    @GetMapping("/delete")
    public String deleteTask(@RequestParam("taskId") @NotNull final String taskId) {
        taskService.remove(taskId);
        return "redirect:/task/list";
    }
}
