package ru.pyshinskiy.tm.api.rest;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.pyshinskiy.tm.dto.DTOConverter;
import ru.pyshinskiy.tm.dto.ProjectDTO;
import ru.pyshinskiy.tm.service.project.IProjectService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/project/")
public class ProjectRestController {

    @Autowired
    private IProjectService projectService;

    @Autowired
    private DTOConverter dtoConverter;

    @GetMapping("/all")
    public ResponseEntity<List<ProjectDTO>> getAll() {
        return ResponseEntity.ok(projectService.findAll().stream().map(e -> dtoConverter.toProjectDTO(e)).collect(Collectors.toList()));
    }

    @GetMapping("{id}")
    public ResponseEntity<ProjectDTO> getProject(@PathVariable("id") @NotNull final String id) {
        return ResponseEntity.ok(dtoConverter.toProjectDTO(projectService.findOne(id)));
    }

    @PostMapping(value = "/save", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<ProjectDTO> saveProject(@RequestBody @NotNull final ProjectDTO projectDTO) {
            projectService.save(dtoConverter.toProject(projectDTO));
            return ResponseEntity.ok(projectDTO);
    }

    @PutMapping(value = "/save", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public void updateProject(@RequestBody @NotNull final ProjectDTO projectDTO) {
        projectService.save(dtoConverter.toProject(projectDTO));
    }

    @DeleteMapping(value = "/delete/{id}")
    public void deleteProject(@PathVariable("id") @NotNull final String id) {
        projectService.remove(id);
    }
}
