package ru.pyshinskiy.tm.api.web;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import ru.pyshinskiy.tm.dto.DTOConverter;
import ru.pyshinskiy.tm.dto.ProjectDTO;
import ru.pyshinskiy.tm.enumerated.Status;
import ru.pyshinskiy.tm.security.UserPrincipal;
import ru.pyshinskiy.tm.service.project.IProjectService;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/project")
public class ProjectController {

    @Autowired
    private IProjectService projectService;

    @Autowired
    private DTOConverter dtoConverter;

    @InitBinder
    public void initBinder(@NotNull final WebDataBinder webDataBinder) {
        @NotNull final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        dateFormat.setLenient(false);
        webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    @GetMapping("/list")
    public String listProjects(@NotNull final Model model) {
        @NotNull final List<ProjectDTO> projects = projectService.findAll().stream().map(e -> dtoConverter.toProjectDTO(e)).collect(Collectors.toList());
        model.addAttribute("projects", projects);
        return "project-list";
    }

    @GetMapping("/add")
    public String showAddingForm(@NotNull final Model model, @AuthenticationPrincipal @NotNull final UserPrincipal userPrincipal) {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setUserId(userPrincipal.getId());
        model.addAttribute("project", project);
        model.addAttribute("statuses", Arrays.stream(Status.values()).map(Enum::toString).collect(Collectors.toList()));
        return "project-form";
    }

    @PostMapping("/save")
    public String saveProject(@ModelAttribute("project") @NotNull final ProjectDTO project) {
        projectService.save(dtoConverter.toProject(project));
        return "redirect:/project/list";
    }

    @GetMapping("/edit")
    public String editProject(@RequestParam("projectId") @NotNull final String projectId, @NotNull final Model model) {
        @Nullable ProjectDTO project = dtoConverter.toProjectDTO(projectService.findOne(projectId));
        model.addAttribute("project", project);
        model.addAttribute("statuses", Arrays.stream(Status.values()).map(Enum::toString).collect(Collectors.toList()));
        return "project-form";
    }

    @GetMapping("/delete")
    public String deleteProject(@RequestParam("projectId") @NotNull final String projectId) {
        projectService.remove(projectId);
        return "redirect:/project/list";
    }
}
