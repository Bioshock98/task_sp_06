package ru.pyshinskiy.tm.api.rest;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.pyshinskiy.tm.dto.DTOConverter;
import ru.pyshinskiy.tm.dto.TaskDTO;
import ru.pyshinskiy.tm.service.task.ITaskService;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/task")
public class TaskRestController {

    @Autowired
    private ITaskService taskService;

    @Autowired
    private DTOConverter dtoConverter;

    @GetMapping("/all")
    public ResponseEntity<List<TaskDTO>> getAll() {
        return ResponseEntity.ok(taskService.findAll().stream().map(e -> dtoConverter.toTaskDTO(e)).collect(Collectors.toList()));
    }

    @GetMapping("{id}")
    public ResponseEntity<TaskDTO> getTask(@PathVariable("id") @NotNull final String id) {
        return ResponseEntity.ok(dtoConverter.toTaskDTO(taskService.findOne(id)));
    }

    @PostMapping(value = "/save", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<TaskDTO> saveTask(@RequestBody @NotNull final TaskDTO taskDTO) {
        taskService.save(dtoConverter.toTask(taskDTO));
        return ResponseEntity.ok(taskDTO);
    }

    @PutMapping(value = "/save", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public void updateTask(@RequestBody @NotNull final TaskDTO taskDTO) {
        taskService.save(dtoConverter.toTask(taskDTO));
    }

    @DeleteMapping(value = "/delete/{id}")
    public void deleteTask(@PathVariable("id") @NotNull final String id) {
        taskService.remove(id);
    }
}
