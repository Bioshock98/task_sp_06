package ru.pyshinskiy.tm.api.web;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.pyshinskiy.tm.dto.DTOConverter;
import ru.pyshinskiy.tm.dto.UserDTO;
import ru.pyshinskiy.tm.enumerated.RoleType;
import ru.pyshinskiy.tm.service.user.IUserService;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private IUserService userService;

    @Autowired
    private DTOConverter dtoConverter;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping("/list")
    public String listUser(@NotNull final Model model) {
        @NotNull final List<UserDTO> users = userService.findAll().stream().map(e -> dtoConverter.toUserDTO(e)).collect(Collectors.toList());
        model.addAttribute("users", users);
        return "user-list";
    }

    @GetMapping("/add")
    public String showAddingForm(@NotNull final Model model) {
        @NotNull final UserDTO user = new UserDTO();
        user.setRoleType(RoleType.USER);
        model.addAttribute("user", user);
        model.addAttribute("roles", Arrays.stream(RoleType.values()).map(Enum::toString).collect(Collectors.toList()));
        return "user-form";
    }

    @PostMapping("/save")
    public String saveUser(@ModelAttribute("user") @NotNull final UserDTO userDTO) {
        userDTO.setPasswordHash(passwordEncoder.encode(userDTO.getPasswordHash()));
        userService.save(dtoConverter.toUser(userDTO));
        return "redirect:/user/list";
    }

    @GetMapping("/edit")
    public String editUser(@RequestParam("userId") @NotNull final String userId, @NotNull final Model model) {
        @Nullable UserDTO user = dtoConverter.toUserDTO(userService.findOne(userId));
        model.addAttribute("user", user);
        model.addAttribute("roles", Arrays.stream(RoleType.values()).map(Enum::toString).collect(Collectors.toList()));
        return "user-form";
    }

    @GetMapping("/delete")
    public String deleteUser(@RequestParam("userId") @NotNull final String userId) {
        userService.remove(userId);
        return "redirect:/user/list";
    }
}
