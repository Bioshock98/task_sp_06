package ru.pyshinskiy.tm.api.web;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.pyshinskiy.tm.dto.DTOConverter;
import ru.pyshinskiy.tm.dto.UserDTO;
import ru.pyshinskiy.tm.service.user.IUserService;

@Controller
@RequestMapping("/")
public class AuthorizationController {

    @Autowired
    private IUserService userService;

    @Autowired
    private DTOConverter dtoConverter;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping("login")
    public String sign_in() {
        return "sign-in";
    }

    @GetMapping("sign_up")
    public String sign_up(@NotNull final Model model) {
        model.addAttribute("user", new UserDTO());
        return "sign-up";
    }

    @PostMapping("sign_up")
    public String register(@ModelAttribute("user") @NotNull final UserDTO user) {
        user.setPasswordHash(passwordEncoder.encode(user.getPasswordHash()));
        userService.save(dtoConverter.toUser(user));
        return "redirect:/login";
    }
}
