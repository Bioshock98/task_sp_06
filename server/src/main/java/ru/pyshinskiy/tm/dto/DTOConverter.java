package ru.pyshinskiy.tm.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.model.Project;
import ru.pyshinskiy.tm.model.Task;
import ru.pyshinskiy.tm.model.User;
import ru.pyshinskiy.tm.service.project.IProjectService;
import ru.pyshinskiy.tm.service.task.ITaskService;
import ru.pyshinskiy.tm.service.user.IUserService;

@Component
public class DTOConverter {

    @Autowired
    private IProjectService projectService;

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IUserService userService;

    @Nullable
    public ProjectDTO toProjectDTO(@Nullable final Project project) {
        if(project == null) return null;
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId(project.getId());
        if(project.getUser() != null) projectDTO.setUserId(project.getUser().getId());
        projectDTO.setName(project.getName());
        projectDTO.setDescription(project.getDescription());
        projectDTO.setStatus(project.getStatus());
        projectDTO.setStartDate(project.getStartDate());
        projectDTO.setFinishDate(project.getFinishDate());
        projectDTO.setCreateTime(project.getCreateTime());
        return projectDTO;
    }

    @Nullable
    public Project toProject(@Nullable final ProjectDTO projectDTO) {
        if(projectDTO == null) return null;
        @NotNull final Project project = new Project();
        project.setId(projectDTO.getId());
        project.setUser(userService.findOne(projectDTO.getUserId()));
        project.setName(projectDTO.getName());
        project.setDescription(projectDTO.getDescription());
        project.setStatus(projectDTO.getStatus());
        project.setStartDate(projectDTO.getStartDate());
        project.setFinishDate(projectDTO.getFinishDate());
        project.setCreateTime(projectDTO.getCreateTime());
        project.setTasks(taskService.findByProject(projectService.findOne(projectDTO.getId())));
        return project;
    }

    @Nullable
    public TaskDTO toTaskDTO(@Nullable final Task task) {
        if(task == null) return null;
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(task.getId());
        taskDTO.setUserId(task.getUser().getId());
        if(task.getProject() != null) {
            taskDTO.setProjectId(task.getProject().getId());
        }
        taskDTO.setName(task.getName());
        taskDTO.setDescription(task.getDescription());
        taskDTO.setStatus(task.getStatus());
        taskDTO.setStartDate(task.getStartDate());
        taskDTO.setFinishDate(task.getFinishDate());
        taskDTO.setCreateTime(task.getCreateTime());
        return taskDTO;
    }

    @Nullable
    public Task toTask(@Nullable final TaskDTO taskDTO) {
        if(taskDTO == null) return null;
        @NotNull final Task task = new Task();
        task.setId(taskDTO.getId());
        task.setUser(userService.findOne(taskDTO.getUserId()));
        task.setProject(projectService.findOne(taskDTO.getProjectId()));
        task.setName(taskDTO.getName());
        task.setDescription(taskDTO.getDescription());
        task.setStatus(taskDTO.getStatus());
        task.setStartDate(taskDTO.getStartDate());
        task.setFinishDate(taskDTO.getFinishDate());
        task.setCreateTime(taskDTO.getCreateTime());
        return task;
    }

    @Nullable
    public User toUser(@Nullable final UserDTO userDTO) {
        if(userDTO == null) return null;
        @NotNull final User user = new User();
        user.setId(userDTO.getId());
        user.setLogin(userDTO.getLogin());
        user.setPasswordHash(userDTO.getPasswordHash());
        user.setRoleType(userDTO.getRoleType());
        user.setProjects(projectService.findAllProjectsByUserId(user.getId()));
        user.setTasks(taskService.findAllTasksByUserId(user.getId()));
        return user;
    }

    @Nullable
    public UserDTO toUserDTO(@Nullable final User user) {
        if(user == null) return null;
        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setLogin(user.getLogin());
        userDTO.setPasswordHash(user.getPasswordHash());
        userDTO.setRoleType(user.getRoleType());
        return userDTO;
    }
}