package ru.pyshinskiy.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.enumerated.RoleType;

@Getter
@Setter
public class UserDTO extends AbstractDTO {

    @NotNull
    private String login;


    @NotNull
    private String passwordHash;

    @NotNull
    private RoleType roleType = RoleType.USER;
}
