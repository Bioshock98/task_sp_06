package ru.pyshinskiy.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.pyshinskiy.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public class AbstractWBSEntity extends AbstractEntity {

    @ManyToOne
    @NotNull
    private User user;

    @NotNull
    @Basic(optional = false)
    private Date createTime = new Date(System.currentTimeMillis());

    @Basic
    @Nullable
    private String name;

    @Basic
    @Nullable
    private String description;

    @Basic(optional = false)
    @Nullable
    @Enumerated(EnumType.STRING)
    private Status status = Status.PLANNED;

    @Basic
    @Nullable
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private Date startDate;

    @Basic
    @Nullable
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private Date finishDate;
}
