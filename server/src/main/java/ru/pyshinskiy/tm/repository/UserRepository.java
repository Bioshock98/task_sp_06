package ru.pyshinskiy.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.pyshinskiy.tm.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    @Nullable
    User findUserById(@NotNull final String id);

    @Nullable
    User findUserByLogin(@NotNull final String login);
}
