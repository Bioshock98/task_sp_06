package ru.pyshinskiy.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.springframework.security.core.GrantedAuthority;

public enum RoleType implements GrantedAuthority {

    USER("USER"),
    ADMINISTRATOR("ADMINISTRATOR"),
    TEST("TEST");

    @NotNull
    private String name;

    RoleType(@NotNull final String name) {
        this.name = name;
    }

    @NotNull
    public String displayName() {
        return name;
    }

    @Override
    public String getAuthority() {
        return name;
    }
}
