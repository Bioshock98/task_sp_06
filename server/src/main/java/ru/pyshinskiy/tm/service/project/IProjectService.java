package ru.pyshinskiy.tm.service.project;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.model.Project;
import ru.pyshinskiy.tm.service.IService;

import java.util.List;

public interface IProjectService extends IService<Project> {

    List<Project> findAllProjectsWithTasks();

    List<Project> findAllProjectsByUserId(@NotNull final String userId);
}
