package ru.pyshinskiy.tm.service.user;

import ru.pyshinskiy.tm.model.User;
import ru.pyshinskiy.tm.service.IService;

public interface IUserService extends IService<User> {
}
