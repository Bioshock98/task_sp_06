package ru.pyshinskiy.tm.service.user;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.pyshinskiy.tm.model.User;
import ru.pyshinskiy.tm.repository.UserRepository;
import ru.pyshinskiy.tm.security.UserPrincipal;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        @Nullable final User user = userRepository.findUserByLogin(login);
        if(user == null) throw new UsernameNotFoundException("User not found");
        return new UserPrincipal(user);
    }
}
