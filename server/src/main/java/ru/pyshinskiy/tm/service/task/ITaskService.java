package ru.pyshinskiy.tm.service.task;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.model.Project;
import ru.pyshinskiy.tm.model.Task;
import ru.pyshinskiy.tm.service.IService;

import java.util.List;

public interface ITaskService extends IService<Task> {

    @NotNull
    List<Task> findByProject(@NotNull final Project project);

    List<Task> findAllTasksByUserId(@NotNull final String userId);
}
