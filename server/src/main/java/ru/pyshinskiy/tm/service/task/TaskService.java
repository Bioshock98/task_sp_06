package ru.pyshinskiy.tm.service.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.pyshinskiy.tm.model.Project;
import ru.pyshinskiy.tm.model.Task;
import ru.pyshinskiy.tm.repository.TaskRepository;
import ru.pyshinskiy.tm.service.AbstractService;

import java.util.List;

@Service
public class TaskService extends AbstractService<Task> implements ITaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Override
    @Nullable
    public Task findOne(@Nullable final String id) {
        return taskRepository.findTaskById(id);
    }

    @Override
    @NotNull
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @NotNull
    public List<Task> findByProject(@Nullable final Project project) {
        return taskRepository.findTasksByProject(project);
    }

    @Override
    @NotNull
    public List<Task> findAllTasksByUserId(@NotNull final String userId) {
        return taskRepository.findTasksByUserId(userId);
    }

    @Transactional
    @Override
    public void save(@Nullable final Task task) {
        taskRepository.save(task);
    }

    @Transactional
    @Override
    public void remove(@Nullable final String taskId) {
        taskRepository.deleteById(taskId);
    }
}
