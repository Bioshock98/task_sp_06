<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
    <title>Task Manager</title>
</head>
<body>
<div class="container">
    <c:url var="login" value="/login"/>
    <a href="${login}">Sign in</a>
    <c:url var="sign_up" value="/sign_up"/>
    <a href="${sign_up}">Sign up</a>
</div>
</body>
</html>
