<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
</head>
<body>
<div class="container text-center">
    <%--@elvariable id="user" type="ru.pyshinskiy.tm.dto.UserDTO"--%>
    <form:form method="POST" action="/user/save" modelAttribute="user" cssClass="d-inline-flex p-2">
        <form:hidden path="id"/>
        <div class="d-inline-flex flex-column justify-content-center">
            <div class="p-2">
                <div class="form-group ">
                    <form:label path="login">Login</form:label>
                    <form:input path="login" cssClass="form-control"/>
                </div>
            </div>
            <div class="p-2">
                <div class="form-group">
                    <form:label path="passwordHash">Password</form:label>
                    <form:input path="passwordHash" cssClass="form-control"/>
                </div>
            </div>
            <div class="p-2">
                <div class="form-group">
                    <form:select path="roleType" cssClass="form-control">
                        <%--@elvariable id="roleTypes" type="java.util.List"--%>
                        <form:options items="${roleTypes}"/>
                    </form:select>
                </div>
            </div>
            <div class="justify-content-center p-2">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    </form:form>
</div>
</body>
</html>
