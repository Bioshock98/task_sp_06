<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
    <title>Project list</title>
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">TaskManager</a>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="<c:url value="/"/>">Dashboard <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="<c:url value="/task/list"/>">Tasks <span class="sr-only">(current)</span></a>
                </li>
            </ul>
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Log out</button>
        </div>
    </nav>
    <div class="container text-center">
        <br />
        <h3 class="text-center">Projects</h3>
        <hr />
        <div class="d-flex justify-content-start">
            <a href="<c:url value="/project/add"/>" class="btn btn-primary active" role="button" aria-pressed="true">Add Project</a>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col"></th>
                    <th scope="col">Name</th>
                    <th scope="col">Description</th>
                    <th scope="col">Status</th>
                    <th scope="col">StartDate</th>
                    <th scope="col">FinishDate</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                <%--@elvariable id="projects" type="java.util.List"--%>
                <c:forEach var="project" items="${projects}">
                    <c:url var="editLink" value="/project/edit">
                        <c:param name="projectId" value="${project.getId()}" />
                    </c:url>
                    <c:url var="deleteLink" value="/project/delete">
                        <c:param name="projectId" value="${project.getId()}" />
                    </c:url>
                    <tr>
                        <th scope="row"></th>
                        <td>${project.getName()}</td>
                        <td>${project.getDescription()}</td>
                        <td>${project.getStatus()}</td>
                        <td><fmt:formatDate value="${project.getStartDate()}" pattern="dd-MM-yyyy" /></td>
                        <td><fmt:formatDate value="${project.getFinishDate()}" pattern="dd-MM-yyyy" /></td>
                        <td>
                            <a href="${editLink}" class="btn btn-secondary btn-sm active" role="button" aria-pressed="true">Edit</a>
                            <a href="${deleteLink}" class="btn btn-secondary btn-sm active" role="button" aria-pressed="true">Delete</a>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </div>
</body>
</html>
