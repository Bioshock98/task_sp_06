<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User List</title>
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">TaskManager</a>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="<c:url value="/"/>">Dashboard <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="<c:url value="/project/list"/>">Projects <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="<c:url value="/task/list"/>">Tasks <span class="sr-only">(current)</span></a>
            </li>
        </ul>
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Log out</button>
    </div>
</nav>
<div class="container text-center">
    <br />
    <h3 class="text-center">Users</h3>
    <hr />
    <div class="d-flex justify-content-start">
        <a href="<c:url value="/user/add"/>" class="btn btn-primary active" role="button" aria-pressed="true">Add User</a>
    </div>
    <table class="table">
        <thead>
            <tr>
                <th scope="col"></th>
                <th scope="col">Login</th>
                <th scope="col">Role</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            <%--@elvariable id="users" type="java.util.List"--%>
            <c:forEach var="user" items="${users}">
                <c:url var="editLink" value="/user/edit">
                    <c:param name="userId" value="${user.getId()}" />
                </c:url>
                <c:url var="deleteLink" value="/user/delete">
                    <c:param name="userId" value="${user.getId()}" />
                </c:url>
                <tr>
                    <th scope="row"></th>
                    <td>${user.getLogin()}</td>
                    <td>${user.getRole()}</td>
                    <td>
                        <a href="${editLink}" class="btn btn-secondary btn-sm active" role="button" aria-pressed="true">Edit</a>
                        <a href="${deleteLink}" class="btn btn-secondary btn-sm active" role="button" aria-pressed="true">Delete</a>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>
</body>
</html>
