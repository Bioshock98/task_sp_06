<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Sign Up</title>
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
</head>
<body>
<div class="container text-center">
    <%--@elvariable id="user" type="ru.pyshinskiy.tm.dto.UserDTO"--%>
    <form:form action="/sign_up" method="post" cssClass="d-inline-flex p-2" modelAttribute="user">
        <form:hidden path="id"/>
        <form:hidden path="roleType"/>
        <div class="d-inline-flex flex-column justify-content-center">
            <div class="form-group p-2">
                <form:label path="login">Email or login</form:label>
                <form:input path="login" class="form-control"/>
            </div>
            <div class="form-group p-2">
                <form:label path="passwordHash">Password</form:label>
                <form:input path="passwordHash" cssClass="form-control"/>
            </div>
            <button class="btn btn-lg btn-primary btn-block p-2" type="submit">Sign up</button>
        </div>
    </form:form>
</div>
</body>
</html>
