<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit Project</title>
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
</head>
<body>
    <div class="container text-center">
        <%--@elvariable id="project" type="ru.pyshinskiy.tm.dto.ProjectDTO"--%>
        <form:form method="POST" action="/project/save" modelAttribute="project" cssClass="d-inline-flex p-2">
            <form:hidden path="id"/>
            <form:hidden path="userId"/>
            <div class="d-inline-flex flex-column justify-content-center">
                <div class="p-2">
                    <div class="form-group ">
                        <form:label path="name">Name</form:label>
                        <form:input path="name" cssClass="form-control"/>
                    </div>
                </div>
                <div class="p-2">
                    <div class="form-group">
                        <form:label path="description">Description</form:label>
                        <form:input path="description" cssClass="form-control"/>
                    </div>
                </div>
                <div class="p-2">
                    <div class="form-group">
                        <form:label path="startDate">Start date</form:label>
                        <form:input path="startDate" cssClass="form-control"/>
                    </div>
                </div>
                <div class="p-2">
                    <div class="form-group">
                        <form:label path="finishDate">Finish date</form:label>
                        <form:input path="finishDate" cssClass="form-control"/>
                    </div>
                </div>
                <div class="p-2">
                    <div class="form-group">
                        <form:select path="status" cssClass="form-control">
                            <%--@elvariable id="statuses" type="java.util.List"--%>
                            <form:options items="${statuses}"/>
                        </form:select>
                    </div>
                </div>
                <div class="justify-content-center p-2">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form:form>
    </div>
</body>
</html>