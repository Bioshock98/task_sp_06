<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Sign In</title>
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
</head>
<body>
    <div class="container text-center">
        <form action="/login/process" method="post" class="d-inline-flex p-2">
            <div class="d-inline-flex flex-column justify-content-center">
                <div class="form-group p-2">
                    <label for="exampleInputEmail1">Email or login</label>
                    <input name="username" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email or login">
                </div>
                <div class="form-group p-2">
                    <label for="exampleInputPassword1">Password</label>
                    <input name="password" type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                </div>
                <button class="btn btn-lg btn-primary btn-block p-2" type="submit">Sign in</button>
            </div>
        </form>
    </div>
</body>
</html>
