<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit Task</title>
    <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet">
</head>
<body>
    <div class="container text-center">
        <%--@elvariable id="task" type="ru.pyshinskiy.tm.dto.TaskDTO"--%>
        <form:form method="POST" action="/task/save" modelAttribute="task" cssClass="d-inline-flex p-2">
            <form:hidden path="id"/>
            <form:hidden path="userId"/>
            <div class="d-inline-flex flex-column justify-content-center">
                <div class="p-2">
                    <div class="form-group ">
                        <form:label path="name">Name</form:label>
                        <form:input path="name" cssClass="form-control"/>
                    </div>
                </div>
                <div class="p-2">
                    <div class="form-group">
                        <form:label path="description">Description</form:label>
                        <form:input path="description" cssClass="form-control"/>
                    </div>
                </div>
                <div class="p-2">
                    <div class="form-group">
                        <form:label path="startDate">Start date</form:label>
                        <form:input path="startDate" cssClass="form-control"/>
                    </div>
                </div>
                <div class="p-2">
                    <div class="form-group">
                        <form:label path="finishDate">Finish date</form:label>
                        <form:input path="finishDate" cssClass="form-control"/>
                    </div>
                </div>
                <div class="p-2">
                    <div class="form-group">
                        <form:select path="status" cssClass="form-control">
                            <%--@elvariable id="statuses" type="java.util.List"--%>
                            <form:options items="${statuses}"/>
                        </form:select>
                    </div>
                </div>
                <div class="p-2">
                    <div class="form-group">
                        <form:select path="projectId" cssClass="form-control">
                            <form:option value="${null}" label="No project"/>
                            <%--@elvariable id="projects" type="java.util.List"--%>
                            <form:options items="${projects}" itemValue="id" itemLabel="name"/>
                        </form:select>
                    </div>
                </div>
                <div class="justify-content-center p-2">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form:form>
    </div>
</body>
</html>
