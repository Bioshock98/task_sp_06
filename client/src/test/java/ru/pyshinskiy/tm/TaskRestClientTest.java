package ru.pyshinskiy.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import ru.pyshinskiy.tm.api.TaskRestClient;
import ru.pyshinskiy.tm.api.UserRestClient;
import ru.pyshinskiy.tm.config.ApplicationConfig;
import ru.pyshinskiy.tm.dto.TaskDTO;
import ru.pyshinskiy.tm.dto.UserDTO;
import ru.pyshinskiy.tm.enumerated.Role;

import java.util.Date;
import java.util.Random;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        classes = { ApplicationConfig.class },
        loader = AnnotationConfigContextLoader.class)
@Category(ru.pyshinskiy.tm.IntegrateRestClientTest.class)
public class TaskRestClientTest {

    @Autowired
    private TaskRestClient taskRestClient;

    @Autowired
    private UserRestClient userRestClient;

    @Nullable
    private UserDTO testUser;

    @Before
    public void setUp() {
        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setLogin("test");
        userDTO.setPasswordHash("1234");
        userDTO.setRole(Role.TEST);
        userRestClient.saveUser(userDTO);
        testUser = userDTO;
    }

    @After
    public void tearDown() {
        userRestClient.deleteUser(testUser.getId());
    }

    @Test
    public void getAllTasks() {
        for(int i = 0; i < 5; i++) {
            taskRestClient.saveTask(createTaskDTO());
        }
        Assert.assertEquals(5, taskRestClient.getAllTasks().size());
    }

    @Test
    public void updateTask() {
        @NotNull final TaskDTO taskDTO = createTaskDTO();
        taskRestClient.saveTask(taskDTO);
        taskDTO.setName("UPDATED");
        taskRestClient.updateTask(taskDTO);
        Assert.assertEquals(taskDTO.getName(), taskRestClient.getTask(taskDTO.getId()).getName());
    }


    @Test
    public void deleteTask() {
        @NotNull final TaskDTO taskDTO = createTaskDTO();
        taskRestClient.saveTask(taskDTO);
        taskRestClient.deleteTask(taskDTO.getId());
        Assert.assertNull(taskRestClient.getTask(taskDTO.getId()));
    }

    private TaskDTO createTaskDTO() {
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setUserId(testUser.getId());
        taskDTO.setName(new Random().toString());
        taskDTO.setDescription("test task description");
        taskDTO.setStartDate(new Date());
        taskDTO.setFinishDate(new Date());
        return taskDTO;
    }
}
