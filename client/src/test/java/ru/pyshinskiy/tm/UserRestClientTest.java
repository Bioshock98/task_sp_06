package ru.pyshinskiy.tm;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import ru.pyshinskiy.tm.api.UserRestClient;
import ru.pyshinskiy.tm.config.ApplicationConfig;
import ru.pyshinskiy.tm.dto.UserDTO;
import ru.pyshinskiy.tm.enumerated.Role;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        classes = { ApplicationConfig.class },
        loader = AnnotationConfigContextLoader.class)
@Category(ru.pyshinskiy.tm.IntegrateRestClientTest.class)
public class UserRestClientTest {

    @Autowired
    private UserRestClient userRestClient;

    @After
    public void tearDown() {
        userRestClient.getAllUsers().stream().filter(e -> e.getRole().equals(Role.TEST)).forEach(e -> userRestClient.deleteUser(e.getId()));
    }

    @Test
    public void getAllUsers() {
        for(int i = 0; i < 5; i++) {
            userRestClient.saveUser(createUserDTO());
        }
        @NotNull final List<UserDTO> testUsers = userRestClient.getAllUsers().stream().filter(e -> e.getRole().equals(Role.TEST)).collect(Collectors.toList());
        Assert.assertEquals(5, testUsers.size());
    }

    @Test
    public void updateUser() {
        @NotNull final UserDTO userDTO = createUserDTO();
        userRestClient.saveUser(userDTO);
        userDTO.setLogin("UPDATED LOGIN");
        userRestClient.updateUser(userDTO);
        Assert.assertEquals(userDTO.getLogin(), userRestClient.getUser(userDTO.getId()).getLogin());
    }


    @Test
    public void deleteUser() {
        @NotNull final UserDTO userDTO = createUserDTO();
        userRestClient.saveUser(userDTO);
        userRestClient.deleteUser(userDTO.getId());
        Assert.assertNull(userRestClient.getUser(userDTO.getId()));
    }

    private UserDTO createUserDTO() {
        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setLogin(new Random().toString());
        userDTO.setPasswordHash("test password");
        userDTO.setRole(Role.TEST);
        return userDTO;
    }
}
