package ru.pyshinskiy.tm.api;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.client.RestTemplate;

@PropertySource("classpath:api.properties")
public abstract class AbstractRestClient {

    @Autowired
    protected RestTemplate restTemplate;

    @Nullable
    @Value("${project.all}")
    protected String projectAllUrl;

    @Nullable
    @Value("${project.get}")
    protected String projectGetUrl;

    @Nullable
    @Value("${project.save}")
    protected String projectSaveUrl;

    @Nullable
    @Value("${project.update}")
    protected String projectUpdateUrl;

    @Nullable
    @Value("${project.delete}")
    protected String projectDeleteUrl;

    @Nullable
    @Value("${task.all}")
    protected String taskAllUrl;

    @Nullable
    @Value("${task.get}")
    protected String taskGetUrl;

    @Nullable
    @Value("${task.save}")
    protected String taskSaveUrl;

    @Nullable
    @Value("${task.update}")
    protected String taskUpdateUrl;

    @Nullable
    @Value("${task.delete}")
    protected String taskDeleteUrl;

    @Nullable
    @Value("${user.all}")
    protected String userAllUrl;

    @Nullable
    @Value("${user.get}")
    protected String userGetUrl;

    @Nullable
    @Value("${user.save}")
    protected String userSaveUrl;

    @Nullable
    @Value("${user.update}")
    protected String userUpdateUrl;

    @Nullable
    @Value("${user.delete}")
    protected String userDeleteUrl;
}
