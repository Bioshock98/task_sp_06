package ru.pyshinskiy.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.enumerated.Role;

@Getter
@Setter
public class UserDTO extends AbstractDTO {

    @Nullable
    private String login;

    @Nullable
    private String passwordHash;

    @Nullable
    private Role role;
}
