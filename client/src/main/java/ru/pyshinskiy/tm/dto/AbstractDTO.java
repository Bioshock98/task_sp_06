package ru.pyshinskiy.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

@Getter
@Setter
public abstract class AbstractDTO {

    @NotNull
    private String id = UUID.randomUUID().toString();
}
